#ifndef HTTP_DOWNLOADER_H
#define HTTP_DOWNLOADER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QJsonDocument>

class HttpDownloader : public QObject
{
    Q_OBJECT
    QNetworkAccessManager *manager;

public:
    explicit HttpDownloader(QObject *parent = nullptr);
    void request(QString url);

private slots:
    QJsonDocument replyFinished(QNetworkReply *reply);
};

#endif // HTTP_DOWNLOADER_H
