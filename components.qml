import QtQuick 2.6
import QtQuick.Controls 2.0 as Controls
import org.kde.kirigami 2.4 as Kirigami

Kirigami.Card {
    id: postCard
    //objectName: "post_card"
    property var stickied_icon: modelData.stickied ? "pin" : NULL

    actions: [
        Kirigami.Action {
            iconName: "arrow-up"
            onTriggered: modelData.score++
        },
        Kirigami.Action {
            text: modelData.score
        },
        Kirigami.Action {
            iconName: "arrow-down"
            onTriggered: modelData.score--
        }
    ]
    hiddenActions: [
        Kirigami.Action {
            text: qsTr("Visit /r/" + modelData.subreddit)
            iconName: "go-home"
            onTriggered: [
                backend.newPage = modelData.subreddit,
                backend.activePage++,
                pageStack.push(mainPageComponent, {message: backend.activePage})
            ]
        },
        Kirigami.Action {
            text: qsTr("Visit /u/" + modelData.author)
            iconName: "user-identity"
        },
        Kirigami.Action {
            text: qsTr("Bookmark post")
            iconName: "bookmark-new"
        }
    ]
    banner {
        source: modelData.image
        title: modelData.title
        titleIcon: stickied_icon
        titleAlignment: Qt.AlignLeft | Qt.AlignBottom
    }
    contentItem: Controls.Label {
        wrapMode: Text.WordWrap
        text: qsTr("/r/" + modelData.subreddit + "  -  /u/" + modelData.author)
    }
}
