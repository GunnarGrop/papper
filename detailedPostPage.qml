import QtQuick 2.6
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.8 as Kirigami

Component {
    id: detailedPostPage
    Kirigami.ScrollablePage {
        id: mainPage
        title: backend.activePageContainer.title
        actions {
            main: Kirigami.Action {
                iconName: sheet.sheetOpen ? "dialog-cancel" : "document-edit"
                onTriggered: {
                    print("Action button in buttons page clicked");
                    sheet.sheetOpen = !sheet.sheetOpen
                }
            }
            contextualActions: [
                Kirigami.Action {
                    text:"Action for buttons"
                    iconName: "bookmarks"
                    onTriggered: print("Action 1 clicked")
                },
                Kirigami.Action {
                    text:"Action 2"
                    iconName: "folder"
                    enabled: false
                },
                Kirigami.Action {
                    text: "Action for Sheet"
                    visible: sheet.sheetOpen
                }
            ]
        }

        Kirigami.OverlaySheet {
            id: sheet
            onSheetOpenChanged: page.actions.main.checked = sheetOpen
            Controls.Label {
                wrapMode: Text.WordWrap
                text: "Lorem ipsum dolor sit amet"
            }
        }

        Kirigami.CardsListView {
            model: backend.activePageContainer.postModelList

            delegate: Kirigami.Card {
                property var stickied_icon: modelData.stickied ? "pin" : NULL

                actions: [
                    Kirigami.Action {
                        iconName: "arrow-up"
                        onTriggered: modelData.score++
                    },
                    Kirigami.Action {
                        text: modelData.score
                    },
                    Kirigami.Action {
                        iconName: "arrow-down"
                        onTriggered: modelData.score--
                    }
                ]
                hiddenActions: [
                    Kirigami.Action {
                        text: qsTr("Visit /r/" + modelData.subreddit)
                        iconName: "go-home"
                        onTriggered: [
                            backend.newPage = modelData.subreddit,
                            backend.activePage++,
                            pageStack.push(mainPageComponent, {message: backend.activePage})
                        ]
                    },
                    Kirigami.Action {
                        text: qsTr("Visit /u/" + modelData.author)
                        iconName: "user-identity"
                    },
                    Kirigami.Action {
                        text: qsTr("Bookmark post")
                        iconName: "bookmark-new"
                    }
                ]
                banner {
                    source: modelData.image
                    title: modelData.title
                    titleIcon: stickied_icon
                    titleAlignment: Qt.AlignLeft | Qt.AlignBottom
                }
                contentItem: Controls.Label {
                    wrapMode: Text.WordWrap
                    text: qsTr("/r/" + modelData.subreddit + "  -  /u/" + modelData.author)
                }
            }
        }
    }
}
