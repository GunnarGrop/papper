import QtQuick 2.6
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.11 as Kirigami
import org.kde.plasma.core 2.0 as PlasmaCore
import com.custom.backend 1.0

Kirigami.ApplicationWindow {
    id: root

    Backend {
        id: backend
    }

    pageStack.globalToolBar.style : Kirigami.ApplicationHeaderStyle.ToolBar

    globalDrawer: Kirigami.GlobalDrawer {
        title: "Hello App"
        titleIcon: "applications-graphics"
        bannerImageSource: "banner.jpg"
        actions: [
            Kirigami.Action {
                text: "View"
                iconName: "view-list-icons"
                Kirigami.Action {
                    text: "action 1"
                }
                Kirigami.Action {
                    text: "action 2"
                }
                Kirigami.Action {
                    text: "action 3"
                }
            },
            Kirigami.Action {
                text: "action 3"
            },
            Kirigami.Action {
                text: "Discover"
                onTriggered: subredditSearchSheet.open()
            }
        ]
        Controls.TextField {
            Layout.fillWidth: true
            placeholderText: qsTr("Enter name")
        }
    }
    
    contextDrawer: Kirigami.ContextDrawer {
        id: contextDrawer
    }

    Kirigami.OverlaySheet {
        id: subredditSearchSheet

        parent: applicationWindow().overlay

        ColumnLayout {
            spacing: Kirigami.Units.largeSpacing * 5
            Layout.preferredWidth:  Kirigami.Units.gridUnit * 25
            Controls.Label {
                Layout.fillWidth: true
                wrapMode: Text.WordWrap
                text: qsTr("Discover subreddits")
            }
            RowLayout {
                Controls.TextField {
                    id: searchField
                    placeholderText: qsTr("Search...")
                    Layout.fillWidth: true
                    onAccepted: [
                        subredditSearchSheet.close(),
                        pageStack.push(searchForSubredditPageComponent, {searchQuery: qsTr(searchField.text)}),
                        searchField.text = ""
                    ]
                }
                Controls.Button {
                    text: "Go"
                    onClicked: [
                        subredditSearchSheet.close(),
                        pageStack.push(searchForSubredditPageComponent, {searchQuery: qsTr(searchField.text)}),
                        searchField.text = ""
                    ]
                }
            }
        }
    }
    
    pageStack.initialPage: mainPageComponent
    Component {
        id: mainPageComponent
        Kirigami.ScrollablePage {
            background: PlasmaCore.ColorScope.backgroundColor
            title: backend.activePageContainer.title
            actions {
                main: Kirigami.Action {
                    iconName: sheet.sheetOpen ? "dialog-cancel" : "document-edit"
                    onTriggered: {
                        print("Action button in buttons page clicked");
                        sheet.sheetOpen = !sheet.sheetOpen
                    }
                }
                contextualActions: [
                    Kirigami.Action {
                        iconName: "view-sort"
                        onTriggered: print("Action 1 clicked")
                    },
                    Kirigami.Action {
                        text:"Action 2"
                        iconName: "folder"
                        enabled: false
                    },
                    Kirigami.Action {
                        text: "Action for Sheet"
                        visible: sheet.sheetOpen
                    }
                ]
            }

            Controls.BusyIndicator {
                Layout.minimumWidth: Units.iconSizes.enormous
                Layout.minimumHeight: width
                running: !backend.activePageContainer.ready;
            }
            
            Kirigami.OverlaySheet {
                id: sheet
                onSheetOpenChanged: page.actions.main.checked = sheetOpen
                Controls.Label {
                    wrapMode: Text.WordWrap
                    text: "Lorem ipsum dolor sit amet"
                }
            }

            Kirigami.CardsListView {
                model: backend.activePageContainer.postModelList

                delegate: Kirigami.Card {
                    property var stickied_icon: modelData.stickied ? "pin" : NULL

                    onClicked: {
                        modelData.loadComments()
                        pageStack.push(detailedPostPageComponent, {commentModel: modelData})
                    }

                    actions: [
                        Kirigami.Action {
                            iconName: "arrow-up"
                            onTriggered: modelData.score++
                        },
                        Kirigami.Action {
                            text: modelData.score
                        },
                        Kirigami.Action {
                            iconName: "arrow-down"
                            onTriggered: modelData.score--
                        }
                    ]
                    hiddenActions: [
                        Kirigami.Action {
                            text: qsTr("Visit /r/" + modelData.subreddit)
                            iconName: "go-home"
                            onTriggered: [
                                backend.newPage = modelData.subreddit,
                                backend.activePage++,
                                pageStack.push(mainPageComponent)
                            ]
                        },
                        Kirigami.Action {
                            text: qsTr("Visit /u/" + modelData.author)
                            iconName: "user"
                            onTriggered: pageStack.push(userPageComponent, {messageTitle: qsTr(modelData.author)})
                        },
                        Kirigami.Action {
                            text: qsTr("Bookmark post")
                            iconName: "bookmark-new"
                        }
                    ]
                    banner {
                        source: modelData.image
                        title: modelData.title
                        titleIcon: stickied_icon
                        titleAlignment: Qt.AlignLeft | Qt.AlignBottom
                        //titleWrapMode: Text.WordWrap
                    }
                    contentItem: Controls.Label {
                        wrapMode: Text.WordWrap
                        text: qsTr("/r/" + modelData.subreddit + "  -  /u/" + modelData.author)
                    }
                }
            }
        }
    }

    Component {
        id: detailedPostPageComponent
        Kirigami.ScrollablePage {
            property string messageTitle
            property var commentModel
            background: PlasmaCore.ColorScope.backgroundColor
            title: model.subreddit
            actions {
                contextualActions: [
                    Kirigami.Action {
                        text:"Action for buttons"
                        iconName: "bookmarks"
                        onTriggered: print("Action 1 clicked")
                    },
                    Kirigami.Action {
                        text:"Action 2"
                        iconName: "folder"
                        enabled: false
                    }
                ]
            }
            Controls.BusyIndicator {
                Layout.minimumWidth: Units.iconSizes.enormous
                Layout.minimumHeight: width
                running: !commentModel.ready;
            }

            ListView {
                id: listView
                spacing: Kirigami.Units.largeSpacing
                headerPositioning: ListView.InlineHeader
                header: Kirigami.Card {
                    id: titleCard
                    width: listView.width
                    z: 1
                    //showClickFeedback: true
                    property var stickied_icon: commentModel.stickied ? "pin" : NULL

                    actions: [
                        Kirigami.Action {
                            iconName: "arrow-up"
                            onTriggered: commentModel.score++
                        },
                        Kirigami.Action {
                            text: commentModel.score
                            onTriggered: commentModel.loadComments()
                        },
                        Kirigami.Action {
                            iconName: "arrow-down"
                            onTriggered: commentModel.score--
                        }
                    ]
                    hiddenActions: [
                        Kirigami.Action {
                            text: qsTr("Visit /r/" + commentModel.subreddit)
                            iconName: "go-home"
                            onTriggered: [
                                backend.newPage = model.subreddit,
                                backend.activePage++,
                                pageStack.push(mainPageComponent)
                            ]
                        },
                        Kirigami.Action {
                            text: qsTr("Visit /u/" + commentModel.author)
                            iconName: "user"
                            onTriggered: pageStack.push(userPageComponent, {messageTitle: qsTr(commentModel.author)})
                        },
                        Kirigami.Action {
                            text: qsTr("Bookmark post")
                            iconName: "bookmark-new"
                        }
                    ]
                    banner {
                        source: commentModel.image
                        title: commentModel.title
                        titleIcon: stickied_icon
                        titleAlignment: Qt.AlignLeft | Qt.AlignBottom
                        titleWrapMode: Text.WordWrap
                    }
                    contentItem: Controls.Label {
                        wrapMode: Text.WordWrap
                        text: qsTr(commentModel.selftext + "\n/r/" + commentModel.subreddit + "  -  /u/" + commentModel.author)
                    }
                }

                model: commentModel.commentList
                delegate: Kirigami.Card {
                    width: listView.width
                    z: 0
                    onClicked: [
                        //visible = false
                    ]
                    banner {
                        title: "/u/" + modelData.author + " - " + modelData.score
                        titleAlignment: Qt.AlignLeft | Qt.AlignBottom
                        titleWrapMode: Text.WordWrap
                        titleLevel: 4
                    }
                    contentItem: Controls.Label {
                        wrapMode: Text.WordWrap
                        text: qsTr(modelData.body)
                    }
                }
            }
        }
    }

    Component {
        id: userPageComponent
        Kirigami.ScrollablePage {
            property string messageTitle
            background: PlasmaCore.ColorScope.backgroundColor
            title: "/u/" + messageTitle
            actions {
                contextualActions: [
                    Kirigami.Action {
                        text:"Action for buttons"
                        iconName: "bookmarks"
                        onTriggered: print("Action 1 clicked")
                    },
                    Kirigami.Action {
                        text:"Action 2"
                        iconName: "folder"
                        enabled: false
                    }
                ]
            }
        }
    }

    Component {
        id: searchForSubredditPageComponent
        Kirigami.ScrollablePage {
            property string searchQuery
            property var searchResults: backend.searchForSubreddit(searchQuery)
            background: PlasmaCore.ColorScope.backgroundColor
            title: "Search: " + searchQuery
            actions {
                contextualActions: [
                    Kirigami.Action {
                        text:"Action for buttons"
                        iconName: "bookmarks"
                        onTriggered: print("Action 1 clicked")
                    },
                    Kirigami.Action {
                        text:"Action 2"
                        iconName: "folder"
                        enabled: false
                    }
                ]
            }
            Kirigami.CardsListView {
                model: searchResults
                delegate:  Kirigami.AbstractCard {
                    onClicked: {
                        backend.newPage = modelData.displayName
                        backend.activePage++
                        pageStack.push(mainPageComponent)
                    }

                    contentItem: Item {
                        implicitWidth: delegateLayout.implicitWidth
                        implicitHeight: delegateLayout.implicitHeight
                        GridLayout {
                            id: delegateLayout
                            anchors {
                                left: parent.left
                                top: parent.top
                                right: parent.right
                                //IMPORTANT: never put the bottom margin
                            }
                            rowSpacing: Kirigami.Units.largeSpacing
                            columnSpacing: Kirigami.Units.largeSpacing
                            columns: width > Kirigami.Units.gridUnit * 20 ? 4 : 2
                            Kirigami.Icon {
                                source: modelData.iconImg
                                Layout.fillHeight: true
                                Layout.preferredWidth: Kirigami.Units.iconSizes.huge
                            }
                            ColumnLayout {
                                Kirigami.Heading {
                                    level: 2
                                    text: modelData.displayName
                                }
                                Kirigami.Separator {
                                    Layout.fillWidth: true
                                }
                                Controls.Label {
                                    Layout.fillWidth: true
                                    wrapMode: Text.WordWrap
                                    text: modelData.publicDescription
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
