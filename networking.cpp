#include "networking.hpp"
#include <QByteArray>
#include <iostream>

HttpDownloader::HttpDownloader(QObject *parent) : QObject(parent)
{

}

void HttpDownloader::request(QString url)
{
    QUrl qurl = url;
    manager = new QNetworkAccessManager(this);

    QObject::connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));

    manager->get(QNetworkRequest(qurl));
}

QJsonDocument HttpDownloader::replyFinished(QNetworkReply *reply)
{
    QString hej = reply->readAll();
    std::cout << hej.toStdString() << std::endl;
    QJsonDocument jsonDocument;
    if(reply->error())
    {
        std::cout << "Error" << std::endl;
        qDebug() << "ERROR!";
        qDebug() << reply->errorString();
    }
    else
    {
        std::cout << "Success" << std::endl;
        qDebug() << reply->header(QNetworkRequest::ContentTypeHeader).toString();
        qDebug() << reply->header(QNetworkRequest::LastModifiedHeader).toDateTime().toString();;
        qDebug() << reply->header(QNetworkRequest::ContentLengthHeader).toULongLong();
        qDebug() << reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        qDebug() << reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();

        QByteArray array = reply->readAll();
        jsonDocument = QJsonDocument::fromJson(array);
    }

    return jsonDocument;
}
