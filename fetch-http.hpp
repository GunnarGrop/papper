#pragma once

#include <iostream>
#include <curl/curl.h>
#include <string>
#include <nlohmann/json.hpp>
#include <QtNetwork>

using json = nlohmann::json;

json fetch_json(std::string reddit_url);
static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp);
