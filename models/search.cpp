#include "search.h"

SearchForSubredditModel::SearchForSubredditModel(QString displayName, QString publicDescription, QString iconImg)
{
    m_displayName = displayName;
    m_publicDescription = publicDescription;
    m_iconImg = iconImg;
}

QString SearchForSubredditModel::displayName()
{
    return m_displayName;
}

QString SearchForSubredditModel::publicDescription()
{
    return m_publicDescription;
}

QString SearchForSubredditModel::iconImg()
{
    return m_iconImg;
}
