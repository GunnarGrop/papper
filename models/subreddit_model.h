#ifndef SUBREDDIT_MODEL_H
#define SUBREDDIT_MODEL_H

#include <QObject>

class SubredditModel : public QObject {

    Q_OBJECT

    Q_PROPERTY(bool ready READ ready NOTIFY readyChanged)
    Q_PROPERTY(QString title READ title)
    Q_PROPERTY(QList<QObject*> postModelList READ postModelList)

public:
    SubredditModel(quint16 pageNo);

    void initialize(QString subreddit);
    bool ready();
    QString title();
    QList<QObject*> postModelList();

    quint16 m_pageNo;

signals:
    void readyChanged();

private:
    QString generate_subreddit_url(QString subreddit);
    bool m_ready;
    QString m_title;
    qint32 m_size;
    QList<QObject*> m_postModelList;
};

#endif // SUBREDDIT_MODEL_H
