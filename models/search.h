#ifndef SEARCH_H
#define SEARCH_H

#include <QObject>

class SearchForSubredditModel : public QObject {

    Q_OBJECT

    Q_PROPERTY(QString displayName READ displayName)
    Q_PROPERTY(QString publicDescription READ publicDescription)
    Q_PROPERTY(QString iconImg READ iconImg)

public:
    SearchForSubredditModel(QString displayName, QString publicDescription, QString iconImg);
    QString displayName();
    QString publicDescription();
    QString iconImg();

private:
    QString m_displayName;
    QString m_publicDescription;
    QString m_iconImg;
};

#endif // SEARCH_H
