#include "reddit_model.h"
#include "subreddit_model.h"
#include "search.h"
#include "fetch-http.hpp"

RedditModel::RedditModel(QObject *parent) : QObject(parent)
{
    m_activePage = 0;
    SubredditModel *c = new SubredditModel(m_activePage);
    m_pageContainers.push_back(c);
    m_pageContainers.last()->initialize("reddit");
}

quint16 RedditModel::activePage()
{
    return m_activePage;
}

void RedditModel::setActivePage(quint16 activePage)
{
    m_activePage = activePage;
    emit activePageChanged(m_activePage);
}

QObject *RedditModel::activePageContainer()
{
    return m_pageContainers.at(m_activePage);
}

void RedditModel::setNewPage(QString newPage)
{
    m_pageContainers.push_back(new SubredditModel(m_activePage));
    m_pageContainers.last()->initialize(newPage);
}

QList<QObject*> RedditModel::searchForSubreddit(QString query)
{
    //Use postModel to store subreddit search info
    QList<QObject*> retval;
    std::string url = "https://www.reddit.com/search.json?q=";
    url.append(query.toUtf8().constData());
    url.append("&type=sr");
    json j_data = fetch_json(url);
    int length = j_data["data"]["children"].size();

    for(int i = 0; i < length; i++)
    {
        if(j_data["data"]["children"][i]["kind"] == "t5")
        {
            if(j_data["data"]["children"][i]["data"]["subreddit_type"] == "public")
            {
                json tmp = j_data["data"]["children"][i]["data"];
                QString dn = QString::fromStdString(tmp["display_name"]);
                QString pd = QString::fromStdString(tmp["public_description"]);
                QString ii = NULL;
                if(!tmp["icon_img"].is_null())
                {
                    ii = QString::fromStdString(tmp["icon_img"]);
                }

                retval.push_back(new SearchForSubredditModel(dn, pd, ii));
            }
        }
    }
    return retval;
}
