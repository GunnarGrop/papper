#include "subreddit_model.h"
#include "post_model.h"
#include "fetch-http.hpp"

SubredditModel::SubredditModel(quint16 pageNo)
{
    m_ready = true;
    m_pageNo = pageNo;
}

void SubredditModel::initialize(QString subreddit)
{
    m_title = subreddit;
    m_size = 0;
    json data = fetch_json(generate_subreddit_url(m_title).toUtf8().constData());
    int length = data["data"]["children"].size();

    for (int i = 0; i < length; ++i) {
        json p = data["data"]["children"][i]["data"];

        //quint64 index = m_size;
        QString subreddit = QString::fromStdString(p["subreddit"]);
        QString title =  QString::fromStdString(p["title"]);
        QString selftext = QString::fromStdString(p["selftext"]);
        //data.selftext_html = p["selftext_html"];
        QString name = QString::fromStdString(p["name"]);
        QString author =  QString::fromStdString(p["author"]);
        bool stickied = p["stickied"];
        quint64 score =  p["score"];
        //quint64 num_comments =  p["num_comments"];
        QString permalink =  QString::fromStdString(p["permalink"]);
        QString url =  QString::fromStdString(p["url"]);
        QString domain =  QString::fromStdString(p["domain"]);
        QString id =  QString::fromStdString(p["id"]);

        QString file_ending = ".jpg";
        if (!url.contains(file_ending)) {
            url = nullptr;
        }

        //posts.push_back(data);
        m_postModelList.push_back(new PostModel(subreddit, title, selftext,
                                                url, stickied, author,
                                                score, permalink));
        m_size++;
    }
}

bool SubredditModel::ready()
{
    return m_ready;
}

QString SubredditModel::title()
{
    return m_title;
}

QList<QObject*> SubredditModel::postModelList()
{
    return m_postModelList;
}

QString SubredditModel::generate_subreddit_url(QString subreddit)
{
    if(subreddit == "reddit")
    {
        return "https://www.reddit.com/.json";
    }
    QString url = "https://www.reddit.com/r/";
    url.append(subreddit);
    url.append("/.json");
    return url;
}
