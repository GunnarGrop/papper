#include "comment_model.h"

CommentModel::CommentModel(QString body, QString body_html, QString name,
                           QString author, qint64 score, qint64 num_replies,
                           QString permalink, QString id, QString parent_id)
{
    //this_comment = ref;
    m_body = body;
    m_author = author;
    m_score = score;
    m_permalink = permalink;
}

QString CommentModel::body()
{
    return m_body;
}

QString CommentModel::author()
{
    return m_author;
}

qint64 CommentModel::score()
{
    return m_score;
}

void CommentModel::setScore(qint64 score)
{
    m_score = score;
    emit scoreChanged(m_score);
}

QList<QObject *> CommentModel::replyList()
{
    return m_replyList;
}
