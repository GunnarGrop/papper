#include "post_model.h"
#include "comment_model.h"
#include "fetch-http.hpp"
#include "networking.hpp"

PostModel::PostModel(QString subreddit, QString title, QString selftext,
                     QString url, bool stickied, QString author,
                     quint64 score, QString permalink)
{
    m_subreddit = subreddit;
    m_title = title;
    m_selftext = selftext;
    m_image = url;
    m_stickied = stickied;
    m_author = author;
    m_score = score;
    m_permalink = permalink;
    m_ready = true;
}

bool PostModel::ready()
{
    return m_ready;
}

void PostModel::setReady(bool status)
{
    m_ready = status;
    emit readyChanged(m_ready);
}

QString PostModel::subreddit()
{
    return m_subreddit;
}

QString PostModel::title()
{
    return m_title;
}

QString PostModel::selftext()
{
    return m_selftext;
}

QString PostModel::image()
{
    return m_image;
}

bool PostModel::stickied()
{
    return m_stickied;
}

QString PostModel::author()
{
    return m_author;
}

qint64 PostModel::score()
{
    return m_score;
}

void PostModel::setScore(qint64 score)
{
    m_score = score;
    emit scoreChanged(m_score);
}

QList<QObject *> PostModel::commentList()
{
    return m_commentList;
}

void PostModel::loadComments()
{
    std::cout << "Download of post started" << std::endl;
    HttpDownloader downloader;
    downloader.request("http://www.reddit.com");

    std::string url = "https://www.reddit.com";
    url.append(m_permalink.toUtf8().constData());
    url.append("/.json");
    json j_data = fetch_json(url);
    int length = j_data[1]["data"]["children"].size();

    for (int i = 0; i < length; ++i) {
        if(j_data[1]["data"]["children"][i]["kind"] == "t1")
        {
            json c = j_data[1]["data"]["children"][i]["data"];
            QString body = QString::fromStdString(c["body"]);
            QString body_html = QString::fromStdString(c["body_html"]);
            QString name = QString::fromStdString(c["name"]);
            QString author = QString::fromStdString(c["author"]);
            quint64 score = c["score"];
            quint64 num_replies = 0;
            if(c["replies"] != "")
            {
                num_replies = c["replies"]["data"]["children"].size();
            }
            QString permalink = QString::fromStdString(c["permalink"]);
            QString id = QString::fromStdString(c["id"]);
            QString parent_id = QString::fromStdString(c["parent_id"]);

            if (num_replies > 0) {
                //fetch_replies(&data, c);
            }

            m_commentList.push_back(new CommentModel(body, body_html, name,
                                                     author, score, num_replies,
                                                     permalink, id, parent_id));
        }
    }
    emit commentListChanged(m_commentList);
}
