#ifndef COMMENT_MODEL_H
#define COMMENT_MODEL_H

#include <QObject>

class CommentModel : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString body READ body)
    Q_PROPERTY(QString author READ author)
    Q_PROPERTY(qint64 score READ score WRITE setScore NOTIFY scoreChanged)
    Q_PROPERTY(QList<QObject*> replyList READ replyList)

public:
    CommentModel(QString body, QString body_html, QString name,
                 QString author, qint64 score, qint64 num_replies,
                 QString permalink, QString id, QString parent_id);

    QString body();
    QString author();
    qint64 score();
    void setScore(qint64 score);
    QList<QObject*> replyList();

signals:
    void scoreChanged(qint64);

private:
    QString m_body;
    QString m_image;
    QString m_author;
    qint64 m_score;
    QString m_permalink;
    QList<QObject*> m_replyList;
};

#endif // COMMENT_MODEL_H
