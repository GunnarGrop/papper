#ifndef POST_MODEL_H
#define POST_MODEL_H

#include <QObject>

class PostModel : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool ready READ ready WRITE setReady NOTIFY readyChanged)
    Q_PROPERTY(QString subreddit READ subreddit)
    Q_PROPERTY(QString title READ title)
    Q_PROPERTY(QString selftext READ selftext)
    Q_PROPERTY(QString image READ image)
    Q_PROPERTY(bool stickied READ stickied)
    Q_PROPERTY(QString author READ author)
    Q_PROPERTY(qint64 score READ score WRITE setScore NOTIFY scoreChanged)
    Q_PROPERTY(QList<QObject*> commentList READ commentList NOTIFY commentListChanged)

public:
    PostModel(QString subreddit, QString title, QString selftext,
              QString url, bool stickied, QString author,
              quint64 score, QString permalink);

    bool ready();
    void setReady(bool status);
    QString subreddit();
    QString title();
    QString selftext();
    QString image();
    bool stickied();
    QString author();
    qint64 score();
    void setScore(qint64 score);
    QList<QObject*> commentList();

signals:
    void readyChanged(bool);
    void scoreChanged(qint64);
    void commentListChanged(QList<QObject*>);

public slots:
    void loadComments();

private:
    bool m_ready;
    QString m_subreddit;
    QString m_title;
    QString m_selftext;
    QString m_image;
    bool m_stickied;
    QString m_author;
    qint64 m_score;
    QString m_permalink;
    QList<QObject*> m_commentList;
};

#endif // POST_MODEL_H
