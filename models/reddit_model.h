#ifndef REDDIT_MODEL_H
#define REDDIT_MODEL_H

#include <QObject>
#include "subreddit_model.h"

class RedditModel : public QObject
{
    Q_OBJECT

    Q_PROPERTY(quint16 activePage READ activePage WRITE setActivePage NOTIFY activePageChanged)
    Q_PROPERTY(QObject* activePageContainer READ activePageContainer NOTIFY activePageContainerChanged)
    Q_PROPERTY(QString newPage WRITE setNewPage)

public:
    explicit RedditModel(QObject *parent = nullptr);

    quint16 activePage();
    void setActivePage(quint16 activePage);
    QObject* activePageContainer();
    void setNewPage(QString newPage);

signals:
    void activePageChanged(quint16);
    void activePageContainerChanged(QObject*);

public slots:
    QList<QObject*> searchForSubreddit(QString query);

private:
    quint16 m_activePage;
    QList<SubredditModel*> m_pageContainers;
};

#endif // REDDIT_MODEL_H
