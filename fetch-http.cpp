#include "fetch-http.hpp"

json fetch_json(std::string reddit_url)
{
  CURL* curl = curl_easy_init();
  CURLcode result;
  std::string read_buffer;
  json retval = NULL;

  if (curl) {
    curl_easy_setopt(curl, CURLOPT_URL, reddit_url.c_str());
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &read_buffer);
    curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1L);
    result = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }

  if (result == CURLE_OK) {
    retval = retval.parse(read_buffer);
  }
  else {
    printf("ERROR: Could not fetch json");
  }
  return retval;
}

static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}
